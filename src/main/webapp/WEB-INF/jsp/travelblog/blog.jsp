<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<!DOCTYPE HTML>
<html lang="ja">

<!--// HEAD //////////////////////////////////////////////////////////-->
<head>
<meta charset="UTF-8">
<link rel="icon" type="image/vnd.microsoft.ico" href="<c:url value="../favicon.ico"/>">
<link rel="shortcut icon" type="image/vnd.microsoft.ico" href="<c:url value="../favicon.ico"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="../css/common.css"/>">
<title>Travel Blog</title>
</head>

<!--// BODY //////////////////////////////////////////////////////////-->
<body>

<header>
	<h1>Travel Blog</h1>
</header>
<hr>

<main>
	
	<p><a href="../index.html">Back to Top Page</a></p>
</main>
<hr>

<footer>
	<p>&copy;2015 shibata_ts All Rights Reserved.</p>
</footer>

<!--// SCRIPT ////////////////////////////////////////////////////////-->
<script type="text/javascript">
<!--

-->
</script>
<!--//////////////////////////////////////////////////////////////////-->
</body>
</html>



