<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<!DOCTYPE HTML>
<html lang="ja">

<!--// HEAD //////////////////////////////////////////////////////////-->
<head>
<meta charset="UTF-8">
<link rel="icon" type="image/vnd.microsoft.ico" href="${contextPath}/favicon.ico">
<link rel="shortcut icon" type="image/vnd.microsoft.ico" href="${contextPath}/favicon.ico">
<link rel="stylesheet" type="text/css" href="${contextPath}/css/common.css">
<link rel="stylesheet" type="text/css" href="${contextPath}/css/index.css">
<title>Error Page</title>
</head>

<!--// BODY //////////////////////////////////////////////////////////-->
<body>

<header>
	<h1>Error Page</h1>
</header>
<hr>

<main>
	<h2><spring:message code="exception.sorry1" /></h2>
	<p>
		<spring:message code="exception.sorry2" /><br>
		<spring:message code="exception.sorry3" />
	</p>
	
	<p>Error : <c:out value="${ex}" /></p>
	
	<p><a href="${contextPath}/index.html">Go to Top Page.</a></p>
</main>
<hr>

<footer>
	<p>&copy;2015 shibata_ts All Rights Reserved.</p>
</footer>

<!--// SCRIPT ////////////////////////////////////////////////////////-->
<script type="text/javascript" src="lib/jquery/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="js/index.js"></script>
<!--//////////////////////////////////////////////////////////////////-->
</body>
</html>



