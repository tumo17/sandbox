<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<!DOCTYPE HTML>
<html lang="ja">

<!--// HEAD //////////////////////////////////////////////////////////-->
<head>
<meta charset="UTF-8">
<link rel="icon" type="image/vnd.microsoft.ico" href="favicon.ico">
<link rel="shortcut icon" type="image/vnd.microsoft.ico" href="favicon.ico">
<link rel="stylesheet" type="text/css" href="css/common.css">
<link rel="stylesheet" type="text/css" href="css/index.css">
<title>GAE Samples</title>
</head>

<!--// BODY //////////////////////////////////////////////////////////-->
<body>

<header>
	<h1>GAE Samples</h1>
</header>
<hr>

<main>
	<h2>Welcome to GoogleAppEngine Samples</h2>
	<table id="index-table">
		<tr>
			<th>Application Name</th>
			<th>Description</th>
		</tr>
		<tr>
			<td class="name"><a href="w4game/input.html">W4Game</a></td>
			<td class="description"><spring:message code="description.w4game" /></td>
		</tr>
		<tr>
			<td class="name"><a href="haiku/howtouse.html">Haiku</a></td>
			<td class="description"><spring:message code="description.haiku" /></td>
		</tr>
		<tr>
			<td class="name"><a href="wakeupmail/display.html">WakeUpMail</a></td>
			<td class="description"><spring:message code="description.wakeupmail" /></td>
		</tr>
		<tr>
			<td class="name"><a href="travelblog/blog.html">TravelBlog</a></td>
			<td class="description"><spring:message code="description.travelblog" /></td>
		</tr>
	</table>
	
	<div id="changeLang">
		<form id="changeLangForm" action="changeLang.html" method="POST">
			<label><input type="radio" name="locale" value="ja" ${locale == 'ja' ? 'checked' : ''}>ja</label>
			<label><input type="radio" name="locale" value="en" ${locale == 'en' ? 'checked' : ''}>en</label>
			<button id="changeLangButton">Change Language</button>
		</form>
	</div>
	
</main>
<hr>

<footer>
	<p>&copy;2015 shibata_ts All Rights Reserved.</p>
</footer>

<!--// SCRIPT ////////////////////////////////////////////////////////-->
<script type="text/javascript" src="lib/jquery/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="js/index.js"></script>
<!--//////////////////////////////////////////////////////////////////-->
</body>
</html>



