<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<!DOCTYPE HTML>
<html lang="ja">

<!--// HEAD //////////////////////////////////////////////////////////-->
<head>
<meta charset="UTF-8">
<link rel="icon" type="image/vnd.microsoft.ico" href="../favicon.ico">
<link rel="shortcut icon" type="image/vnd.microsoft.ico" href="../favicon.ico">
<link rel="stylesheet" type="text/css" href="../css/common.css">
<link rel="stylesheet" type="text/css" href="../css/w4game/input.css">
<title>W4Game</title>
</head>

<!--// BODY //////////////////////////////////////////////////////////-->
<body>

<header>
	<h1>W4Game</h1>
</header>
<hr>

<main>
	<form:form modelAttribute="w4Bean" action="output.html" method="POST">
		<div class="box">
			<dl>
				<dt>When</dt>
				<dd><form:input type="text" path="when" /></dd>
				<dt>Where</dt>
				<dd><form:input type="text" path="where" /></dd>
				<dt>Who</dt>
				<dd><form:input type="text" path="who" /></dd>
				<dt>What</dt>
				<dd><form:input type="text" path="what" /></dd>
			</dl>
		</div>
		<p><input type="submit" value="Create new sentence"></p>
	</form:form>
	
	<p><a href="../index.html">Back to Top Page</a></p>
</main>
<hr>

<footer>
	<p>&copy;2015 shibata_ts All Rights Reserved.</p>
</footer>

<!--// SCRIPT ////////////////////////////////////////////////////////-->
<script type="text/javascript">
<!--

-->
</script>
<!--//////////////////////////////////////////////////////////////////-->
</body>
</html>



