<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<!DOCTYPE HTML>
<html lang="ja">

<!--// HEAD //////////////////////////////////////////////////////////-->
<head>
<meta charset="UTF-8">
<link rel="icon" type="image/vnd.microsoft.ico" href="../favicon.ico">
<link rel="shortcut icon" type="image/vnd.microsoft.ico" href="../favicon.ico">
<link rel="stylesheet" type="text/css" href="../css/common.css">
<title>W4Game</title>
</head>

<!--// BODY //////////////////////////////////////////////////////////-->
<body>

<header>
	<h1>W4Game</h1>
</header>
<hr>

<main>
	<div class="box">
		<c:out value="${sentence}" />
	</div>
	<p><a href="input.html">Try again</a></p>
</main>
<hr>

<footer>
	<p>&copy;2015 shibata_ts All Rights Reserved.</p>
</footer>

<!--// SCRIPT ////////////////////////////////////////////////////////-->
<script type="text/javascript">
<!--

-->
</script>
<!--//////////////////////////////////////////////////////////////////-->
</body>
</html>



