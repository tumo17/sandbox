<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<!DOCTYPE HTML>
<html lang="ja">

<!--// HEAD //////////////////////////////////////////////////////////-->
<head>
<meta charset="UTF-8">
<link rel="icon" type="image/vnd.microsoft.ico" href="<spring:url value="../favicon.ico"/>">
<link rel="shortcut icon" type="image/vnd.microsoft.ico" href="<spring:url value="../favicon.ico"/>">
<link rel="stylesheet" type="text/css" href="<spring:url value="../css/common.css"/>">
<link rel="stylesheet" type="text/css" href="<spring:url value="../css/wakeupmail/display.css"/>">
<title>WakeUp</title>
</head>

<!--// BODY //////////////////////////////////////////////////////////-->
<body>

<header>
	<h1>WakeUp</h1>
</header>
<hr>

<main>
	<h2>WakeUp Mail</h2>
	<p>Let's register WakeUp information !!</p>
	
	<form:form modelAttribute="wakeUpForm" action="register.html" method="POST">
		<div class="d-box">
			<dl>
				<dt>MailAddress:</dt>
				<dd>
					<form:input path="email" cssErrorClass="errStyle" placeHolder="test@mail.com" maxLength="40" /><br>
					<form:errors cssClass="errStyle" path="email" />
				</dd>
			</dl>
			<dl>
				<dt>Name:</dt>
				<dd>
					<form:input path="userName" cssErrorClass="errStyle" placeHolder="Spike Spiegel" maxLength="40" /><br>
					<form:errors cssClass="errStyle" path="userName" />
				</dd>
			</dl>
			<dl>
				<dt>WakeUp Time:</dt>
				<dd>
					<form:select path="wakeupYear">
						<c:forEach begin="${defYear}" end="${defYear + 10}" var="i">
							<form:option value="${i}" label="${i}" />
						</c:forEach>
					</form:select>
					<form:select path="wakeupMonth">
						<c:forEach begin="1" end="12" var="i">
							<form:option value="${i}" label="${i}" />
						</c:forEach>
					</form:select>
					<form:select path="wakeupDate">
						<c:forEach begin="1" end="31" var="i">
							<form:option value="${i}" label="${i}" />
						</c:forEach>
					</form:select>
					<form:select path="wakeupHour">
						<c:forEach begin="0" end="23" var="i">
							<form:option value="${i}" label="${i}" />
						</c:forEach>
					</form:select>
					<form:select path="wakeupMinute">
						<c:forEach begin="0" end="59" step="5" var="i">
							<form:option value="${i}" label="${i}" />
						</c:forEach>
					</form:select>
				</dd>
			</dl>
		</div>
		
		<p><input type="submit" value="Register WakeUp Mail"></p>
	</form:form>
	
	<p><a href="../index.html">Back to Top Page</a></p>
</main>
<hr>

<footer>
	<p>&copy;2015 shibata_ts All Rights Reserved.</p>
</footer>

<!--// SCRIPT ////////////////////////////////////////////////////////-->
<script type="text/javascript">
<!--

-->
</script>
<!--//////////////////////////////////////////////////////////////////-->
</body>
</html>



