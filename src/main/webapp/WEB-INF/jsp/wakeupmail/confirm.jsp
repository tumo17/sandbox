<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<!DOCTYPE HTML>
<html lang="ja">

<!--// HEAD //////////////////////////////////////////////////////////-->
<head>
<meta charset="UTF-8">
<link rel="icon" type="image/vnd.microsoft.ico" href="../favicon.ico">
<link rel="shortcut icon" type="image/vnd.microsoft.ico" href="../favicon.ico">
<link rel="stylesheet" type="text/css" href="../css/common.css">
<title>WakeUp</title>
</head>

<!--// BODY //////////////////////////////////////////////////////////-->
<body>

<header>
	<h1>WakeUp</h1>
</header>
<hr>

<main>
	<h2>WakeUp Mail</h2>
	<p>Let's register WakeUp information !!</p>
	
	<p><a href="../index.html">Back to Top Page</a></p>
</main>
<hr>

<footer>
	<p>&copy;2015 shibata_ts All Rights Reserved.</p>
</footer>

<!--// SCRIPT ////////////////////////////////////////////////////////-->
<script type="text/javascript">
<!--

-->
</script>
<!--//////////////////////////////////////////////////////////////////-->
</body>
</html>



