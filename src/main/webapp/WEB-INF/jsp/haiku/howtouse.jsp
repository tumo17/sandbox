<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<!DOCTYPE HTML>
<html lang="ja">

<!--// HEAD //////////////////////////////////////////////////////////-->
<head>
<meta charset="UTF-8">
<link rel="icon" type="image/vnd.microsoft.ico" href="../favicon.ico">
<link rel="shortcut icon" type="image/vnd.microsoft.ico" href="../favicon.ico">
<link rel="stylesheet" type="text/css" href="../css/common.css">
<title>Haiku</title>
</head>

<!--// BODY //////////////////////////////////////////////////////////-->
<body>

<header>
	<h1>Haiku</h1>
</header>
<hr>

<main>
	<h2>Members</h2>
	<table id="member-table">
		<tr><th>Name</th><th>Mail Address</th></tr>
		<tr><td>Spike Spiegel</td><td>spike@cloudhaiku.appsoptchat.com</td></tr>
		<tr><td>Jet Black</td><td>jet@cloudhaiku.appsoptchat.com</td></tr>
		<tr><td>Faye Valentine</td><td>faye@cloudhaiku.appsoptchat.com</td></tr>
	</table>
	
	<h2>How to use this service</h2>
	<div>
		<p>
			Cloud Haiku Club has 3 members.
		</p>
		<p>
			Let's chat to them by Google Talk or something<br>
			which uses XMPP protocol.
		</p>
		<p>
			Then, they answer it, singing a Haiku.
		</p>
	</div>
	
	<p><a href="../index.html">Back to Top Page</a></p>
</main>
<hr>

<footer>
	<p>&copy;2015 shibata_ts All Rights Reserved.</p>
</footer>

<!--// SCRIPT ////////////////////////////////////////////////////////-->
<script type="text/javascript">
<!--

-->
</script>
<!--//////////////////////////////////////////////////////////////////-->
</body>
</html>



