package com.appspot.shibatats.app.wakeupmail.front.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.appspot.shibatats.app.wakeupmail.back.dao.AlarmDao;
import com.appspot.shibatats.app.wakeupmail.back.model.Alarm;
import com.appspot.shibatats.app.wakeupmail.front.form.WakeUpForm;
import com.appspot.shibatats.app.wakeupmail.front.util.WakeUpUtil;

@Controller
@RequestMapping(value = "/wakeupmail")
public class WakeUpMailController {

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private Validator wakeUpFormValidator;

	@Autowired
	private AlarmDao alarmDao;

	@ModelAttribute
	public WakeUpForm setUp() {
		WakeUpForm wakeUpForm = new WakeUpForm();
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"));
		wakeUpForm.setWakeupYear(cal.get(Calendar.YEAR));
		wakeUpForm.setWakeupMonth(cal.get(Calendar.MONTH) + 1);
		wakeUpForm.setWakeupDate(cal.get(Calendar.DATE));
		wakeUpForm.setWakeupHour(cal.get(Calendar.HOUR_OF_DAY));
		wakeUpForm.setWakeupMinute(WakeUpUtil.getNextFive(cal.get(Calendar.MINUTE)));
		return wakeUpForm;
	}

	@RequestMapping(value = "/display")
	public ModelAndView display() {
		ModelAndView mav = new ModelAndView();
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"));
		mav.addObject("defYear", cal.get(Calendar.YEAR));
		return mav;
	}

	@RequestMapping(value = "/register")
	public ModelAndView register(WakeUpForm wakeUpForm, BindingResult bindingResult, HttpSession session) {
		wakeUpFormValidator.validate(wakeUpForm, bindingResult);
		if (bindingResult.hasErrors()) {
			ModelAndView mav = new ModelAndView("wakeupmail/display");
			return mav;
		}

		Alarm alarm = getAlarm(wakeUpForm);
		alarmDao.createAlarm(alarm);
		System.out.println(alarm);

		ModelAndView mav = new ModelAndView("wakeupmail/confirm");
		mav.addObject("alarm", alarm);
		return mav;
	}

	private Alarm getAlarm(WakeUpForm wakeUpForm) {
		String email = wakeUpForm.getEmail();
		String userName = wakeUpForm.getUserName();
		Date wakeupTime = parseWakeUpTime(wakeUpForm);
		Alarm alarm = new Alarm(email, userName, wakeupTime);
		return alarm;
	}

	private Date parseWakeUpTime(WakeUpForm wakeUpForm) {
		Date wakeupDate = null;
		int year = wakeUpForm.getWakeupYear();
		int month = wakeUpForm.getWakeupMonth() - 1;
		int date = wakeUpForm.getWakeupDate();
		int hour = wakeUpForm.getWakeupHour();
		int minute = wakeUpForm.getWakeupMinute();
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"));
		cal.setLenient(false);
		cal.set(year, month, date, hour, minute, 0);
		try {
			wakeupDate = cal.getTime();
		} catch (IllegalArgumentException e) {
			// TODO do something
		}
		return wakeupDate;
	}

}
