package com.appspot.shibatats.app.wakeupmail.back.dao.impl;

import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import org.springframework.stereotype.Repository;

import com.appspot.shibatats.app.wakeupmail.back.dao.AlarmDao;
import com.appspot.shibatats.app.wakeupmail.back.model.Alarm;
import com.appspot.shibatats.system.pmf.PMF;

@Repository
public class AlarmDaoJdo implements AlarmDao {

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Alarm> findAllAlarmBeforeCurrentTime() {
		List<Alarm> alarmList = null;

		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			Query query = pm.newQuery(Alarm.class);
			query.setFilter("wakeupDate <= :currentDate");
			query.setOrdering("wakeupDate");
			alarmList = (List<Alarm>)query.execute(new Date());
		} finally {
			pm.close();
		}

		return alarmList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Alarm findAlarm(String email) {
		Alarm alarm = null;

		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			alarm = pm.getObjectById(Alarm.class, email);
		} finally {
			pm.close();
		}

		return alarm;
	}

	/**
	 * {@inheritDoc}<br>
	 * updateAlarmと処理内容は同じだが、Method名でCreate処理を行っていることを明示的に宣言する
	 */
	@Override
	public void createAlarm(Alarm alarm) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			pm.makePersistent(alarm);
		} finally {
			pm.close();
		}
	}

	/**
	 * {@inheritDoc}<br>
	 * createAlarmと処理内容は同じだが、Method名でUpdate処理を行っていることを明示的に宣言する
	 */
	@Override
	public void updateAlarm(Alarm alarm) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			pm.makePersistent(alarm);
		} finally {
			pm.close();
		}
	}

	/**
	 * {@inheritDoc}<br>
	 * 本アプリケーションで一番はまった。<br>
	 * 実装内でGetした後Deleteしている。<br>
	 * DatastoreのTransaction管理上、同じPersistentManager内でgetしたAlarmのみ削除できるという仕様。<br>
	 */
	@Override
	public void deleteAlarm(String email) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			Alarm alarm = pm.getObjectById(Alarm.class, email);
			pm.deletePersistent(alarm);
		} finally {
			pm.close();
		}
	}

}
