package com.appspot.shibatats.app.wakeupmail.front.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.appspot.shibatats.app.wakeupmail.back.dao.AlarmDao;
import com.appspot.shibatats.app.wakeupmail.back.dao.impl.AlarmDaoJdo;
import com.appspot.shibatats.app.wakeupmail.back.model.Alarm;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions.Builder;

public class WakeUpServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	AlarmDao alarmDao = new AlarmDaoJdo();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		List<Alarm> alarmList = alarmDao.findAllAlarmBeforeCurrentTime();
		for (Alarm alarm : alarmList) {
			Queue queue = QueueFactory.getQueue("wakeup-queue");
			queue.add(Builder.withUrl("/task/wakeupmail").param("email", alarm.getEmail()));
		}
	}

}
