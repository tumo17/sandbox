package com.appspot.shibatats.app.wakeupmail.front.util;

public class WakeUpUtil {

	private WakeUpUtil() {
	}

	public static int getNextFive(int target) {
		int remainder = target % 5;
		int result = target + (5 - remainder);
		result = result >= 60 ? 55 : result;
		return result;
	}
}
