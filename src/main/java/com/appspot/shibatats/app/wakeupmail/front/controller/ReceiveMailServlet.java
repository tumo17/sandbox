package com.appspot.shibatats.app.wakeupmail.front.controller;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

import javax.jdo.JDOObjectNotFoundException;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.appspot.shibatats.app.wakeupmail.back.dao.AlarmDao;
import com.appspot.shibatats.app.wakeupmail.back.dao.impl.AlarmDaoJdo;

public class ReceiveMailServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(ReceiveMailServlet.class.getName());

	private AlarmDao alarmDao = new AlarmDaoJdo();

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		Session mailSession = Session.getDefaultInstance(new Properties(), null);
		String email = "";
		try {
			MimeMessage msg = new MimeMessage(mailSession, req.getInputStream());
			InternetAddress address = new InternetAddress(msg.getFrom()[0].toString());
			email = address.getAddress().toLowerCase();
		} catch (MessagingException e) {
			logger.warning(e.toString());
			e.printStackTrace();
		}

		try {
			alarmDao.deleteAlarm(email);
		} catch (JDOObjectNotFoundException e) {
			logger.warning(e.toString());
		}
	}

}
