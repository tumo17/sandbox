package com.appspot.shibatats.app.wakeupmail.back.dao;

import java.util.List;

import com.appspot.shibatats.app.wakeupmail.back.model.Alarm;

public interface AlarmDao {

	/**
	 * 現在時間以前のAlarmを全て取得
	 * @return Alarmのリスト
	 */
	List<Alarm> findAllAlarmBeforeCurrentTime();

	/**
	 * Alarmを取得する
	 * @param email
	 * @return Alarm
	 */
	Alarm findAlarm(String email);

	/**
	 * Alarmを作成する
	 * @param alarm
	 */
	void createAlarm(Alarm alarm);

	/**
	 * Alarmを更新する
	 * @param alarm
	 */
	void updateAlarm(Alarm alarm);

	/**
	 * Alarmを削除する
	 * @param email 削除対象のEmailアドレス(PK)
	 */
	void deleteAlarm(String email);
}
