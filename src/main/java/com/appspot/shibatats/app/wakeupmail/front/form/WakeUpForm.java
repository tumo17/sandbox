package com.appspot.shibatats.app.wakeupmail.front.form;


public class WakeUpForm {

	private String email;
	private String userName;
	private int wakeupYear;
	private int wakeupMonth;
	private int wakeupDate;
	private int wakeupHour;
	private int wakeupMinute;

	// Getter & Setter /////////////////////////////////////////////////////////////////////////////////////////////////
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getWakeupYear() {
		return wakeupYear;
	}
	public void setWakeupYear(int wakeupYear) {
		this.wakeupYear = wakeupYear;
	}
	public int getWakeupMonth() {
		return wakeupMonth;
	}
	public void setWakeupMonth(int wakeupMonth) {
		this.wakeupMonth = wakeupMonth;
	}
	public int getWakeupDate() {
		return wakeupDate;
	}
	public void setWakeupDate(int wakeupDate) {
		this.wakeupDate = wakeupDate;
	}
	public int getWakeupHour() {
		return wakeupHour;
	}
	public void setWakeupHour(int wakeupHour) {
		this.wakeupHour = wakeupHour;
	}
	public int getWakeupMinute() {
		return wakeupMinute;
	}
	public void setWakeupMinute(int wakeupMinute) {
		this.wakeupMinute = wakeupMinute;
	}

	// toString ////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String toString() {
		return "WakeUpForm [email=" + email + ", userName=" + userName + ", wakeupYear=" + wakeupYear
				+ ", wakeupMonth=" + wakeupMonth + ", wakeupDate=" + wakeupDate + ", wakeupHour=" + wakeupHour
				+ ", wakeupMinute=" + wakeupMinute + "]";
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
