package com.appspot.shibatats.app.wakeupmail.back.model;

import java.util.Date;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

/**
 * @author tumo
 *
 */
@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Alarm {

	@PrimaryKey
	private String email;

	@Persistent
	private String userName;

	@Persistent
	private Date wakeupDate;

	@Persistent
	private int cnt;

	// Constructor /////////////////////////////////////////////////////////////////////////////////////////////////////
	public Alarm(String email, String userName, Date wakeupDate) {
		this.email = email;
		this.userName = userName;
		this.wakeupDate = wakeupDate;
	}

	// Getter & Setter /////////////////////////////////////////////////////////////////////////////////////////////////
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getWakeupDate() {
		return wakeupDate;
	}

	public void setWakeupDate(Date wakeupDate) {
		this.wakeupDate = wakeupDate;
	}

	public int getCnt() {
		return cnt;
	}

	public void setCnt(int cnt) {
		this.cnt = cnt;
	}

	// toString ////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String toString() {
		return "Alerm [email=" + email + ", userName=" + userName + ", wakeupDate=" + wakeupDate + ", cnt=" + cnt + "]";
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
