package com.appspot.shibatats.app.wakeupmail.front.controller;

import java.io.IOException;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.appspot.shibatats.app.wakeupmail.back.dao.AlarmDao;
import com.appspot.shibatats.app.wakeupmail.back.dao.impl.AlarmDaoJdo;
import com.appspot.shibatats.app.wakeupmail.back.model.Alarm;
import com.google.appengine.api.mail.MailService;
import com.google.appengine.api.mail.MailService.Message;
import com.google.appengine.api.mail.MailServiceFactory;

public class SendMailServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(SendMailServlet.class.getName());

	AlarmDao alarmDao = new AlarmDaoJdo();

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		try {
			execute(req);
		} catch (Exception e) {
			logger.severe(e.toString());
			e.printStackTrace();
		}
	}

	private void execute(HttpServletRequest req) throws IOException {
		String email = req.getParameter("email");
		Alarm alarm = alarmDao.findAlarm(email);
		sendMail(alarm);
		updateAlarm(alarm);
	}

	private static final String LS = System.lineSeparator();

	private void sendMail(Alarm alarm) throws IOException {
		String email = alarm.getEmail();
		String userName = alarm.getUserName();
		int cnt = alarm.getCnt();

		Message msg = new Message();
		msg.setSender("shibatats@shibatats-sandbox.appspotmail.com");
		msg.setTo(email);
		if (cnt == 0) {
			msg.setSubject("It's Time!");
			msg.setTextBody("Hi " + userName + "!" + LS + "It's time! Wake up!!");
		} else {
			msg.setSubject("Emergency!!!");
			msg.setTextBody("Hey " + userName + "!!" + LS + "It's time after 5 minutes!!" + LS);
		}
		MailService mailService = MailServiceFactory.getMailService();
		mailService.send(msg);
	}

	private void updateAlarm(Alarm alarm) {
		int cnt = alarm.getCnt();
		if (cnt == 0) {
			alarm.setCnt(cnt + 1);
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"));
			cal.add(Calendar.MINUTE, 5);
			alarm.setWakeupDate(cal.getTime());
			alarmDao.updateAlarm(alarm);
		} else {
			alarmDao.deleteAlarm(alarm.getEmail());
		}
	}

}
