package com.appspot.shibatats.app.wakeupmail.front.util;

import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.appspot.shibatats.app.wakeupmail.front.form.WakeUpForm;

public class WakeUpFormValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return WakeUpForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		WakeUpForm wakeUpForm = (WakeUpForm)target;
		if (!StringUtils.hasLength(wakeUpForm.getEmail())) {
			errors.rejectValue("email", "error.required");
		}
		if (!StringUtils.hasLength(wakeUpForm.getUserName())) {
			errors.rejectValue("userName", "error.required");
		}

		if (errors.hasErrors()) {
			errors.reject("error.input.wakeUpForm");
		}
	}

}
