package com.appspot.shibatats.app.haiku.back.bot;

public interface Bot {
	String makeHaiku();
}
