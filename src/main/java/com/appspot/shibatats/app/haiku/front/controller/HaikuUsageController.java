package com.appspot.shibatats.app.haiku.front.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/haiku")
public class HaikuUsageController {

	@RequestMapping(value = "/howtouse")
	public ModelAndView display() {
		return new ModelAndView();
	}

}
