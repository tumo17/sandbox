package com.appspot.shibatats.app.haiku.back.bot;

import java.util.HashMap;
import java.util.Map;

public class BotFactory {

	private static final BotFactory instance = new BotFactory();
	private BotFactory() {
	}
	public static BotFactory getInstance() {
		return instance;
	}

	private static Map<String, Bot> botMap = new HashMap<>();
	static {
		botMap.put("spike", new SpikeBot());
		botMap.put("jet", new JetBot());
		botMap.put("faye", new FayeBot());
	}

	public static Bot get(String id) {
		return botMap.get(id);
	}

}
