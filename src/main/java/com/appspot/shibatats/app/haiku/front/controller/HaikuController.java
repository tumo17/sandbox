package com.appspot.shibatats.app.haiku.front.controller;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.appspot.shibatats.app.haiku.back.bot.Bot;
import com.appspot.shibatats.app.haiku.back.bot.BotFactory;
import com.google.appengine.api.xmpp.JID;
import com.google.appengine.api.xmpp.Message;
import com.google.appengine.api.xmpp.MessageBuilder;
import com.google.appengine.api.xmpp.SendResponse;
import com.google.appengine.api.xmpp.SendResponse.Status;
import com.google.appengine.api.xmpp.XMPPService;
import com.google.appengine.api.xmpp.XMPPServiceFactory;

public class HaikuController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(HaikuUsageController.class.getName());

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		XMPPService xmppService = XMPPServiceFactory.getXMPPService();
		Message rcvMsg = xmppService.parseMessage(req);

		JID toJid = rcvMsg.getFromJid();
		for (JID fromJid : rcvMsg.getRecipientJids()) {
			String fromJidText = fromJid.getId();
			Bot bot = BotFactory.get(fromJidText.split("@")[0]);
			if (bot == null) {
				logger.warning("There is no " + fromJidText);
				break;
			}
			String haiku = bot.makeHaiku();
			Message msg = new MessageBuilder().withFromJid(fromJid).withRecipientJids(toJid).withBody(haiku).build();
			Status status = xmppService.sendMessage(msg).getStatusMap().get(toJid);
			if (!SendResponse.Status.SUCCESS.equals(status)) {
				logger.warning("Sending Message from " + fromJid.getId() + " to " + toJid.getId() + " is failed.");
			}
		}
	}

}
