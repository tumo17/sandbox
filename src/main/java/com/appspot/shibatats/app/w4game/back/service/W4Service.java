package com.appspot.shibatats.app.w4game.back.service;

import com.appspot.shibatats.app.w4game.back.model.W4Bean;

public interface W4Service {
	String makeSentence(W4Bean w4Bean);
}
