package com.appspot.shibatats.app.w4game.back.service.impl;

import org.springframework.stereotype.Service;

import com.appspot.shibatats.app.w4game.back.model.W4Bean;
import com.appspot.shibatats.app.w4game.back.service.W4Service;
import com.appspot.shibatats.app.w4game.front.util.SentenceUtil;

@Service
public class W4ServiceImpl implements W4Service {

	@Override
	public String makeSentence(W4Bean w4Bean) {
		String when = w4Bean.getWhen();
		String where = w4Bean.getWhere();
		String who = w4Bean.getWho();
		String what = w4Bean.getWhat();
		return SentenceUtil.makeSentence(when, where, who, what);
	}

}
