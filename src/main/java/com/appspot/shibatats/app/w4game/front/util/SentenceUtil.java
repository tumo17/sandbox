package com.appspot.shibatats.app.w4game.front.util;

public class SentenceUtil {

	private SentenceUtil() {
	}

	private static final String[] whenArray = { "さっき", "機能", "あれはもう３年も前", "紀元前", "ジュラ紀" };
	private static final String[] whereArray = { "自宅で", "居酒屋で", "世界の中心で", "プロジェクト会議で", "火星で" };
	private static final String[] whoArray = { "私が", "あんたが", "おやじが", "アイドルが", "神様が" };
	private static final String[] whatArray = { "こけた", "悟りを開いた", "笑った", "10円拾った", "だっふんだ" };

	public static String makeSentence(String when, String where, String who, String what) {
		StringBuilder result = new StringBuilder();
		result.append("".equals(when) ? choose(whenArray) : when).append(" ");
		result.append("".equals(where) ? choose(whereArray) : where).append(" ");
		result.append("".equals(who) ? choose(whoArray) : who).append(" ");
		result.append("".equals(what) ? choose(whatArray) : what).append(" ");
		return result.toString();
	}

	private static String choose(String[] array) {
		return array[(int)(Math.random() * array.length)];
	}

}
