package com.appspot.shibatats.app.w4game.front.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.appspot.shibatats.app.w4game.back.model.W4Bean;
import com.appspot.shibatats.app.w4game.back.service.W4Service;

@Controller
@RequestMapping(value = "/w4game")
public class W4GameController {

	@Autowired
	private W4Service w4Service;

	@ModelAttribute
	public W4Bean setup() {
		W4Bean w4Bean = new W4Bean();
		return w4Bean;
	}

	@RequestMapping(value = "/input")
	public ModelAndView index() {
		return new ModelAndView();
	}

	@RequestMapping(value = "/output")
	public ModelAndView post(W4Bean w4Bean) {
		ModelAndView mav = new ModelAndView("w4game/output");
		String sentence = w4Service.makeSentence(w4Bean);
		mav.addObject("sentence", sentence);
		return mav;
	}

}
