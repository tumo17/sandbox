package com.appspot.shibatats.app.travelblog.front.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/travelblog")
public class TravelBlogController {

	@RequestMapping(value = "/blog")
	public ModelAndView blog() throws Exception {
		return new ModelAndView();
	}

}
