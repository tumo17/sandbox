package com.appspot.shibatats.system.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

	@RequestMapping(value = "index")
	public ModelAndView index(HttpSession session) {
		String locale = (String)session.getAttribute("locale");
		locale = (locale == null) ? "ja" : locale;

		ModelAndView mav = new ModelAndView();
		mav.addObject("locale", locale);
		return mav;
	}

}
