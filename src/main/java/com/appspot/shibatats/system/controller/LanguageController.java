package com.appspot.shibatats.system.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LanguageController {

	@RequestMapping(value = "changeLang")
	public void changeLang(HttpServletRequest req, HttpServletResponse res, HttpSession session) throws IOException {
		String locale = req.getParameter("locale");
		session.setAttribute("locale", locale);
		res.sendRedirect("index.html");
	}

}
