package com.appspot.shibatats.system.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

public class GlobalExceptionResolver implements HandlerExceptionResolver {

	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {

		String requestURL = request.getRequestURL().toString();
		String requestURI = request.getRequestURI();
		String contextPath = requestURL.replaceAll(requestURI, "");

		ModelAndView mav = new ModelAndView("exception");
		mav.addObject("contextPath", contextPath);
		mav.addObject("ex", ex);
		return mav;
	}

}
